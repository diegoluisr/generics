package co.edu.uniquindio.integer;

import java.util.Iterator;

public class Iterador implements Iterator<NodoEntero> {

    private NodoEntero actual;

    public Iterador(NodoEntero cabeza) {
        this.actual = cabeza;
    }

    @Override
    public boolean hasNext() {
        if (actual != null) {
            return true;
        }
        return false;
    }

    @Override
    public NodoEntero next() {
        if (actual != null) {
            NodoEntero resultado = actual;
            actual = actual.getSiguiente();
            return resultado;
        }
        return null;
    }

    @Override
    public void remove() {
        throw new UnsupportedOperationException();
    }
}
