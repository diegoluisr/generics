package co.edu.uniquindio.integer;

import java.util.Iterator;

public class ListaEntera implements Iterable<NodoEntero> {

    private NodoEntero cabeza;

    public ListaEntera() {
    }

    public ListaEntera(NodoEntero cabeza) {
        this.cabeza = cabeza;
    }

    public void agregar(NodoEntero nodo) {
        if (cabeza == null) {
            cabeza = nodo;
        }
        else {
            NodoEntero actual = cabeza;
            while (actual.getSiguiente() != null) {
                actual = actual.getSiguiente();
            }
            actual.setSiguiente(nodo);
            nodo.setAnterior(actual);
        }
    }

    @Override
    public Iterator<NodoEntero> iterator() {
//        Iterator<Nodo> iterador = new Iterador(this.cabeza);
        Iterator<NodoEntero> iterador = new Iterator<>() {

            private NodoEntero actual = cabeza;

            @Override
            public boolean hasNext() {
                if (actual != null) {
                    return true;
                }
                return false;
            }

            @Override
            public NodoEntero next() {
                if (actual != null) {
                    NodoEntero resultado = actual;
                    actual = actual.getSiguiente();
                    return resultado;
                }
                return null;
            }

            @Override
            public void remove() {
                throw new UnsupportedOperationException();
            }
        };
        return iterador;
    }
}
