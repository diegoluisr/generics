package co.edu.uniquindio.integer;

public class NodoEntero {

    private Integer t;
    private NodoEntero siguiente;
    private NodoEntero anterior;

    public NodoEntero(Integer t) {
        this.t = t;
    }

    public Integer getValor() {
        return t;
    }

    public void setValor(Integer t) {
        this.t = t;
    }

    public NodoEntero getSiguiente() {
        return siguiente;
    }

    public void setSiguiente(NodoEntero siguiente) {
        this.siguiente = siguiente;
    }

    public NodoEntero getAnterior() {
        return anterior;
    }

    public void setAnterior(NodoEntero anterior) {
        this.anterior = anterior;
    }
}
