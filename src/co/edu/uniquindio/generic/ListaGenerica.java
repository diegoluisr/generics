package co.edu.uniquindio.generic;

import java.util.Iterator;

public class ListaGenerica<T> implements Iterable<NodoGenerico<T>> {

    private NodoGenerico<T> cabeza;

    public ListaGenerica() {
    }

    public ListaGenerica(NodoGenerico<T> cabeza) {
        this.cabeza = cabeza;
    }

    public void agregar(NodoGenerico<T> nodo) {
        if (cabeza == null) {
            cabeza = nodo;
        }
        else {
            NodoGenerico actual = cabeza;
            while (actual.getSiguiente() != null) {
                actual = actual.getSiguiente();
            }
            actual.setSiguiente(nodo);
            nodo.setAnterior(actual);
        }
    }

    @Override
    public Iterator<NodoGenerico<T>> iterator() {
        Iterator<NodoGenerico<T>> iterador = new Iterator<>() {

            private NodoGenerico<T> actual = cabeza;

            @Override
            public boolean hasNext() {
                if (actual != null) {
                    return true;
                }
                return false;
            }

            @Override
            public NodoGenerico next() {
                if (actual != null) {
                    NodoGenerico<T> resultado = actual;
                    actual = actual.getSiguiente();
                    return resultado;
                }
                return null;
            }

            @Override
            public void remove() {
                throw new UnsupportedOperationException();
            }
        };
        return iterador;
    }
}
