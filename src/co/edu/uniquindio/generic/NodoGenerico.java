package co.edu.uniquindio.generic;

public class NodoGenerico<T> {

    private T t;
    private NodoGenerico siguiente;
    private NodoGenerico anterior;

    public NodoGenerico(T t) {
        this.t = t;
    }

    public T getValor() {
        return t;
    }

    public void setValor(T t) {
        this.t = t;
    }

    public NodoGenerico getSiguiente() {
        return siguiente;
    }

    public void setSiguiente(NodoGenerico siguiente) {
        this.siguiente = siguiente;
    }

    public NodoGenerico getAnterior() {
        return anterior;
    }

    public void setAnterior(NodoGenerico anterior) {
        this.anterior = anterior;
    }
}
