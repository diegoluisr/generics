package co.edu.uniquindio.string;

public class NodoCadena {

    private String t;
    private NodoCadena siguiente;
    private NodoCadena anterior;

    public NodoCadena(String t) {
        this.t = t;
    }

    public String getValor() {
        return t;
    }

    public void setValor(String t) {
        this.t = t;
    }

    public NodoCadena getSiguiente() {
        return siguiente;
    }

    public void setSiguiente(NodoCadena siguiente) {
        this.siguiente = siguiente;
    }

    public NodoCadena getAnterior() {
        return anterior;
    }

    public void setAnterior(NodoCadena anterior) {
        this.anterior = anterior;
    }
}
