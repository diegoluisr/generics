package co.edu.uniquindio.string;

import java.util.Iterator;

public class ListaCadena implements Iterable<NodoCadena> {

    private NodoCadena cabeza;

    public ListaCadena() {
    }

    public ListaCadena(NodoCadena cabeza) {
        this.cabeza = cabeza;
    }

    public void agregar(NodoCadena nodo) {
        if (cabeza == null) {
            cabeza = nodo;
        }
        else {
            NodoCadena actual = cabeza;
            while (actual.getSiguiente() != null) {
                actual = actual.getSiguiente();
            }
            actual.setSiguiente(nodo);
            nodo.setAnterior(actual);
        }
    }

    @Override
    public Iterator<NodoCadena> iterator() {
        Iterator<NodoCadena> iterador = new Iterator<>() {

            private NodoCadena actual = cabeza;

            @Override
            public boolean hasNext() {
                if (actual != null) {
                    return true;
                }
                return false;
            }

            @Override
            public NodoCadena next() {
                if (actual != null) {
                    NodoCadena resultado = actual;
                    actual = actual.getSiguiente();
                    return resultado;
                }
                return null;
            }

            @Override
            public void remove() {
                throw new UnsupportedOperationException();
            }
        };
        return iterador;
    }
}
