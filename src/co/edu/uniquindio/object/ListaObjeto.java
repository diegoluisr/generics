package co.edu.uniquindio.object;

import java.util.Iterator;

public class ListaObjeto implements Iterable<NodoObjeto> {

    private NodoObjeto cabeza;

    public ListaObjeto() {
    }

    public ListaObjeto(NodoObjeto cabeza) {
        this.cabeza = cabeza;
    }

    public void agregar(NodoObjeto nodo) {
        if (cabeza == null) {
            cabeza = nodo;
        }
        else {
            NodoObjeto actual = cabeza;
            while (actual.getSiguiente() != null) {
                actual = actual.getSiguiente();
            }
            actual.setSiguiente(nodo);
            nodo.setAnterior(actual);
        }
    }

    @Override
    public Iterator<NodoObjeto> iterator() {
        Iterator<NodoObjeto> iterador = new Iterator<>() {

            private NodoObjeto actual = cabeza;

            @Override
            public boolean hasNext() {
                if (actual != null) {
                    return true;
                }
                return false;
            }

            @Override
            public NodoObjeto next() {
                if (actual != null) {
                    NodoObjeto resultado = actual;
                    actual = actual.getSiguiente();
                    return resultado;
                }
                return null;
            }

            @Override
            public void remove() {
                throw new UnsupportedOperationException();
            }
        };
        return iterador;
    }
}
