package co.edu.uniquindio.object;

public class NodoObjeto {

    private Object t;
    private NodoObjeto siguiente;
    private NodoObjeto anterior;

    public NodoObjeto(Object t) {
        this.t = t;
    }

    public Object getValor() {
        return t;
    }

    public void setValor(Object t) {
        this.t = t;
    }

    public NodoObjeto getSiguiente() {
        return siguiente;
    }

    public void setSiguiente(NodoObjeto siguiente) {
        this.siguiente = siguiente;
    }

    public NodoObjeto getAnterior() {
        return anterior;
    }

    public void setAnterior(NodoObjeto anterior) {
        this.anterior = anterior;
    }
}
