package co.edu.uniquindio;


import co.edu.uniquindio.generic.ListaGenerica;
import co.edu.uniquindio.generic.NodoGenerico;
import co.edu.uniquindio.object.ListaObjeto;
import co.edu.uniquindio.object.NodoObjeto;
import co.edu.uniquindio.string.ListaCadena;
import co.edu.uniquindio.string.NodoCadena;

import java.util.*;

public class Main {

    public static void main(String[] args) {
        /*ListaObjeto listaObjeto = new ListaObjeto();
        listaObjeto.agregar(new NodoObjeto(1));
        listaObjeto.agregar(new NodoObjeto("Hola"));
        listaObjeto.agregar(new NodoObjeto(3));
        listaObjeto.agregar(new NodoObjeto("Mundo"));
        listaObjeto.agregar(new NodoObjeto(5));
        listaObjeto.agregar(new NodoObjeto(6));

        for (NodoObjeto nodo: listaObjeto) {
            System.out.println(nodo.getValor());
        }

        System.out.println(listaObjeto);

        ListaCadena listaCadena = new ListaCadena();
        listaCadena.agregar(new NodoCadena("1"));
        listaCadena.agregar(new NodoCadena("Hola"));
        listaCadena.agregar(new NodoCadena("3"));
        listaCadena.agregar(new NodoCadena("Mundo"));
        listaCadena.agregar(new NodoCadena("5"));
        listaCadena.agregar(new NodoCadena("6"));

        for (NodoObjeto nodo: listaObjeto) {
            System.out.println(nodo.getValor());
        }

        System.out.println(listaObjeto);

        ListaGenerica<Object> listaGenerica = new ListaGenerica<>();
        listaGenerica.agregar(new NodoGenerico<>("Hola"));
        listaGenerica.agregar(new NodoGenerico<>(1));
        listaGenerica.agregar(new NodoGenerico<>(new ArrayList<String>()));

        ListaGenerica<String> listaGenericaCadenas = new ListaGenerica<>();
        listaGenericaCadenas.agregar(new NodoGenerico<>("Hola"));
        listaGenericaCadenas.agregar(new NodoGenerico<>("Mundo"));
        listaGenericaCadenas.agregar(new NodoGenerico<>("!"));*/

        // Key = Llave
        // Value = Valor

//        HashMap<String, Integer> datos = new HashMap<>();
//        datos.put("Hola", 10);
//        datos.put("Mundo", 15);
//
//        for (Map.Entry<String, Integer> elemento: datos.entrySet()) {
//            System.out.println(elemento.getKey() + ": " + elemento.getValue());
//        }

        HashMap<Integer, ArrayList<String>> datosComlejos = new HashMap<>();
        datosComlejos.put(1, new ArrayList<>(Arrays.asList("Hola Mundo!")));
        datosComlejos.put(1, new ArrayList<>(Arrays.asList("Hola Mundo cruel!")));
        datosComlejos.put(2, new ArrayList<>(Arrays.asList("Hola", "Mundo", "!")));

        for (Map.Entry<Integer, ArrayList<String>> elemento: datosComlejos.entrySet()) {
            System.out.println(elemento.getKey() + ": " + elemento.getValue());
        }
    }

}
